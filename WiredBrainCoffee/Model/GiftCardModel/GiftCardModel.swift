//
//  GiftCardModel.swift
//  WiredBrainCoffee
//
//  Created by iulian david on 29/09/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

struct GiftCardModel {
    var id: UUID
    var description: String
    var image: UIImage
}
