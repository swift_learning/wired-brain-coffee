//
//  GiftCollectionViewCell.swift
//  WiredBrainCoffee
//
//  Created by iulian david on 29/09/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class GiftCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 10
        imageView.layer.cornerRadius = layer.cornerRadius
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 1, height: 1)
    }
    
    func setup(giftCardModel: GiftCardModel) {
        imageView.image = giftCardModel.image
    }
}
