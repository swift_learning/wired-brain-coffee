//
//  GiftViewDataSource.swift
//  WiredBrainCoffee
//
//  Created by iulian david on 29/09/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class GiftDataSource: NSObject, UICollectionViewDataSource {
    
    var seasonalGiftCards = [GiftCardModel]()
    
    var dataChanged: (() -> Void)?
    
    override init() {
        super.init()
        GiftCardFunctions.getSeasonalGiftCards { [weak self] (downloadedSeasonalGiftCards) in
            self?.seasonalGiftCards = downloadedSeasonalGiftCards
            self?.dataChanged?()
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seasonalGiftCards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCardCell", for: indexPath)
            as? GiftCollectionViewCell else {
                return UICollectionViewCell()
        }
        cell.setup(giftCardModel: seasonalGiftCards[indexPath.item])
        return cell
    }
}
