//
//  GiftViewController.swift
//  WiredBrainCoffee
//
//  Created by iulian david on 29/09/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class GiftViewController: UIViewController {
    
    let datasource = GiftDataSource()
    
    @IBOutlet weak var seasonalCollectionView: UICollectionView!
    
    @IBOutlet weak var seasonalCollectionHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        seasonalCollectionView.dataSource = datasource
        datasource.dataChanged = { [weak self] in
            self?.seasonalCollectionView.reloadData()
        }
        seasonalCollectionView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setHeightOfCollectionView()
    }
    
    func setHeightOfCollectionView() {
        let width = seasonalCollectionView.bounds.width - 30
        let height = width / 1.5
        seasonalCollectionHeight.constant = height
    }
}

extension GiftViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = seasonalCollectionView.bounds.width - 50
        let height = width / 1.5
        return CGSize(width: width, height: height)
    }
    
    
    
}

